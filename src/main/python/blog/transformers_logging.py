# Suppress the informative and verbose logging from transformers trainer
import logging

import transformers

for module in [
    transformers.configuration_utils,
    transformers.modeling_utils,
    transformers.tokenization_utils_base,
    transformers.trainer,
]:
    getattr(module, "logger").setLevel(logging.ERROR)
