.PHONY: FORCE

#################################################################################
# GLOBALS                                                                       #
#################################################################################

PROJECT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
PROJECT_NAME = blog
HOSTNAME ?= $(shell hostname)

UID ?= $(shell id -u)
GID ?= $(shell id -g)
QUARTO_VERSION = $(shell yq .variables.QUARTO_VERSION version.yaml)
DOCKER_COMPOSE = UID=$(UID) GID=$(GID) QUARTO_VERSION=$(QUARTO_VERSION) docker-compose

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Start jupyterlab to edit the site
edit : FORCE .make/requirements
	PYTHONPATH=$(PWD)/src/main/python \
		nohup poetry run jupyter lab \
			--no-browser \
			--ip 0.0.0.0 \
		>/dev/null 2>&1 &

## Show running jupyter instances
list : FORCE .make/requirements
	poetry run jupyter server list

## Install poetry requirements
requirements : .make/requirements

.make/requirements : pyproject.toml poetry.lock
	PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring poetry env use $(shell cat .python-version)
	PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring poetry install
	touch $@

## start (or restart) the services
blog/start : FORCE
	$(DOCKER_COMPOSE) down --remove-orphans || true
	rm .quarto/preview/lock || true
	$(DOCKER_COMPOSE) up --detach preview

## stop all containers
blog/stop : FORCE
	$(DOCKER_COMPOSE) stop

## remove all containers
blog/remove : FORCE
	$(DOCKER_COMPOSE) stop || true
	$(DOCKER_COMPOSE) rm || true

## build or rebuild the services WITHOUT cache
blog/build : FORCE
	$(DOCKER_COMPOSE) stop || true
	$(DOCKER_COMPOSE) rm || true
	$(DOCKER_COMPOSE) build --force-rm --no-cache

## Render site without watching
blog/convert : FORCE
	$(DOCKER_COMPOSE) up convert

FORCE :

#################################################################################
# PROJECT RULES                                                                 #
#################################################################################



#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := show-help
# See <https://gist.github.com/klmr/575726c7e05d8780505a> for explanation.
.PHONY: show-help
show-help:
	@echo "$$(tput setaf 2)Available rules:$$(tput sgr0)";sed -ne"/^## /{h;s/.*//;:d" -e"H;n;s/^## /---/;td" -e"s/:.*//;G;s/\\n## /===/; s/\\n//g;p;}" ${MAKEFILE_LIST}|awk -F === -v n=$$(tput cols) -v i=4 -v a="$$(tput setaf 6)" -v z="$$(tput sgr0)" '{printf"- %s%s%s\n",a, $$1,z;m=split($$2,w,"---");l=n-i;for(j=1;j<=m;j++){l-=length(w[j])+1;if(l<= 0){l=n-i-length(w[j])-1;}printf"%*s%s\n",-i," ",w[j];}}'
