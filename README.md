Quarto Blog Framework for GitLab
--------------------------------

This is the framework for using Quarto on Gitlab.

### Requirements

This requires:

 * poetry >= 1.5.1
 * yq (`sudo snap install yq`)
 * docker and docker-compose

### Blogging

All blog posts are jupyter notebooks.
Consult the running section for details on starting jupyter.

New posts should go in the `posts/` folder.
You should put them in a dated set of subfolders, e.g. `posts/2023/06/15/my-awesome-post.ipynb`.
The corresponding website url for this would be `/posts/2023/06/15/my-awesome-post.html`.

#### Post Metadata
[docs](https://quarto.org/docs/tools/jupyter-lab.html#yaml-front-matter)

The first cell in a post should be a *raw* cell which has the following content:

```
---
title: "My Awesome Post"
date: "2023-06-15"
description: "An awesome post"
bibliography: "../../../references.bib"
---
```

You can specify arbitrary additional post level options.
To hide the post from the index, sitemap and search use `draft: true` as a top level option:

```
---
title: "My Awesome Post"
date: "2023-06-15"
description: "An awesome post"
bibliography: "../../../references.bib"
draft: true
---
```

Drafts can still be found by their exact url.

#### Citations
[docs](https://quarto.org/docs/visual-editor/technical.html#citations)

These are included using the `[@TAG]` notation, for example `[@vaswani2017attention]`.
These are based on the bibtex references defined in the `posts/references.bib` file.
These can be copied from arxiv by clicking on the `Export BibTeX Citation` button.
They look like:

```
@misc{vaswani2017attention,
    title={Attention Is All You Need},
    author={Ashish Vaswani and Noam Shazeer and Niki Parmar and Jakob Uszkoreit and Llion Jones and Aidan N. Gomez and Lukasz Kaiser and Illia Polosukhin},
    year={2017},
    eprint={1706.03762},
    archivePrefix={arXiv},
    primaryClass={cs.CL}
}
```

The opening text in the curly braces is the tag and can be any unique word.

#### Cell Options
[docs](https://quarto.org/docs/reference/formats/html.html#code)

There are directives that can be added to any cell to control the inclusion of that cell in the blog.
These are added as a comment to the start of the cell.
By default all code is folded, to turn that off you can add this to your cell:

```python
#| code-fold: false

print("You really should see this")
```

### Running

There are two parts to running this.
The jupyterlab instance which allows you to create and edit posts.
The quarto container which will perfom live renders of your blog.

#### Jupyter Lab

Start the jupyter instance with:

```bash
make edit
```

*The output from this is redirected to prevent interruptions if you close the terminal*.
You can find the server url with token by running:

```bash
make list
```

#### Quarto

Start the blog with:

```bash
make blog/start
```

This will stop any running instance so it can be used to restart the blog.
You will be able to see the blog on [http://localhost:4000](http://localhost:4000) once this has completed.

If you have a problem with the docker container you can rebuild it with

```bash
make blog/build
```

This will not use the docker cache.

### Upgrading

Quarto is updated regularly.
You can check the latest release on [their github page](https://github.com/quarto-dev/quarto-cli).
To use the latest version update the `version.yaml` file and alter the `QUARTO_VERSION` variable.
This will require a rebuild of your docker image (should happen when you next start the blog) and will be picked up by the gitlab CI runner.


### Python Source Code

I have created an extension which allows you to add python source code to your posts.
This is automatically set up and works both in jupyter and in the blog.
The blog will replace the import with the source code of the imported file, including a comment indicating the original source.

This means that:

```python
from blog import transformers_logging
```

and

```python
import blog.transformers_logging
```

become

```
# from src/main/python/blog/transformers_logging.py

# Suppress the informative and verbose logging from transformers trainer
import logging

import transformers

for module in [
    transformers.configuration_utils,
    transformers.modeling_utils,
    transformers.tokenization_utils_base,
    transformers.trainer,
]:
    getattr(module, "logger").setLevel(logging.ERROR)
```

It's important to realise that the entire file is copied in but _not_ any other transitively imported files.
Furthermore the `from ... import ...` style of import still results in the entire file, which could lead to unreproducible code (e.g. if you redefine a variable within that file).

The aim of this is to allow code reuse between posts while maintaining the completeness of the posts themselves.
Each blog post should have complete and accurate code that could be run to reproduce the results that you report.
