-- This should just replace `from blog... import blah` with the source code of that file.
-- This should only occur when that is the only content

function path(_package)
    return _package:gsub("%.", "/") .. ".py"
end

function lines_from(file)
    file = os.getenv("PWD") .. "/src/main/python/" .. file
    local lines = {}
    for line in io.lines(file) do
        lines[#lines + 1] = line
    end
    return lines
end

function package_from(line)
    _package, _imports = string.match(line, "from (blog%..*) import (.*)")
    if _package ~= nil then
        return _package
    end

    _package = string.match(line, "import (blog%..*)")
    return _package
end

-- https://pandoc.org/lua-filters.html#type-codeblock
function CodeBlock(el)
    text = el.text
    replacement = nil
    for line in string.gmatch(text, "([^\n]+)") do
        if line == "" then
            goto continue
        end

        _package = package_from(line)
        if _package == nil then
            return el
        end

        file = path(_package)
        code = "# from src/main/python/" .. file .. "\n" .. table.concat(lines_from(file), "\n")
        if replacement == nil then
            replacement = code
        else
            replacement = replacement .. "\n\n\n\n" .. code
        end

        ::continue::
    end
    el.text = replacement
    return el
end
